import numpy as np
from scipy.signal import tukey
from scipy.interpolate import interp1d
from math import *

"""
This file is used as a library in the tiler.py file.
Here we define the mandatory functions used to whiten the signal just to make the creation of the spectrogram easier.
"""
# define the sampling frequency [Hz]
fs = 1024


def spectrum4s(signal, t0):
    """This function returns the FT of a 4s interval"""
    y = signal[t0 * fs:(t0 + 4) * fs]

    n_window = len(y)
    tukey_w = tukey(n_window, 0.5)

    fft = np.fft.rfft(y * tukey_w)

    return fft


def whitening(hfile=True):
    """This function returns the GPS time of the 4s interval where the gravitational wave is observed
    and the whitened spectrum of this interval.
    It uses the data from H1 if hfile=True and from L1 otherwise."""

    # point out the file
    if hfile:
        file = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.00.npy")
        file1 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.01.npy")
        file2 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.02.npy")
        file3 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.03.npy")
        file4 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.04.npy")
        file5 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.05.npy")
        file6 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.06.npy")
        file7 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.07.npy")

        f = open("asdH.txt", 'r')
    else:
        file = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/l1.data.00.npy")
        file1 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/l1.data.01.npy")
        file2 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/l1.data.02.npy")
        file3 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/l1.data.03.npy")
        file4 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/l1.data.04.npy")
        file5 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/l1.data.05.npy")
        file6 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/l1.data.06.npy")
        file7 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/l1.data.07.npy")

        f = open("asdL.txt", 'r')

    # get x and y axis
    x_buff = np.concatenate((file[0], file1[0], file2[0], file3[0], file4[0], file5[0], file6[0], file7[0]), axis=None)
    y_buff = np.concatenate((file[1], file1[1], file2[1], file3[1], file4[1], file5[1], file6[1], file7[1]), axis=None)

    # convert to numpy array (starting from now you can forget that ROOT exist)
    x_arr = np.array(x_buff)
    y_arr = np.array(y_buff)

    """Compute the FFT"""

    # get back the stored ASD values
    N = f.readlines()
    f.close()

    # creation of an interpolated function to get the ASD value for any frequency
    n = []
    for i in range(len(N)):
        n.append(float(N[i]))

    fre_asd = np.linspace(0, fs / 2, len(n))
    interp = interp1d(fre_asd, n)

    gw = [0, 0]

    fre = np.linspace(0, fs / 2, 2 * fs + 1)
    cut20 = 20 * int(len(fre) / fre[-1])  # position of the 20Hz frequency in the list
    spectra = np.zeros((int(((x_arr[-1] - x_arr[0]) / 2)),
                        len(fre)),
                       dtype=np.dtype(complex))  # matrix used to store a whitened spectrum for each 4s interval
    for i in range(int(((x_arr[-1] - x_arr[0]) / 2))):  # loop to move 2s by 2s
        A1 = spectrum4s(y_arr, 2 * i)
        new_A1 = np.zeros(len(fre), dtype=np.dtype(complex))
        new_A1[cut20:] = A1[cut20:]  # all frequencies below 20Hz go to zero
        A1_white = sqrt(2) * new_A1 / interp(fre)  # whitening of the signal
        signal = np.fft.irfft(A1_white)
        for j in range(len(fre)):
            spectra[i][j] = A1_white[j]
        for k in range(4 * fs):  # search for the gravitational wave
            if signal[k] > gw[1]:
                gw[0] = i
                gw[1] = signal[k]  # we store the 4s interval with the highest peak in its whitened signal

    return [x_arr[2 * gw[0]], spectra[gw[0]]]
