import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hann


def define_plot_resolution():
    """
    This function allow to define the resolution of a matplotlib plot on a way
    wich is device independent. Put this before saving any of your plot to get
    homogeneous resolution.
    """

    fig = plt.gcf()  # get current figure

    DPI = fig.get_dpi()
    fig.set_size_inches(1920.0 / float(DPI), 1080.0 / float(DPI))

    return


# This commande turn matplotlib interactive mode off.
# Plot will not be desplayed as long as it is activate
# Note that plot are still saved with it
plt.ioff()

# point out the root file
file = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.00.npy")
file1 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.01.npy")
file2 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.02.npy")
file3 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.03.npy")
file4 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.04.npy")
file5 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.05.npy")
file6 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.06.npy")
file7 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.07.npy")

# get x and y axis
x_buff = np.concatenate((file[0], file1[0], file2[0], file3[0], file4[0], file5[0], file6[0], file7[0]), axis=None)
y_buff = np.concatenate((file[1], file1[1], file2[1], file3[1], file4[1], file5[1], file6[1], file7[1]), axis=None)

# plot the signal
plt.figure(1)
plt.plot(x_buff, y_buff)
plt.title('Signal', fontsize=20)
plt.xlabel('gps time [s]', fontsize=20)
plt.ylabel('amplitude', fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
define_plot_resolution()
plt.savefig('signal.png')
# plt.show()

# convert to numpy array (starting from now you can forget that ROOT exist)
x_arr = np.array(x_buff)
y_arr = np.array(y_buff)

""" Compute the FFT """
# define the sampling frequency [Hz]
fs = 1024

# define the frequency array
freq = np.linspace(0, fs / 2, int(len(x_arr) / 2))

# compute the fft
fft = np.fft.fft(y_arr)

# keep the "interesting" part of this fft for the following
fft_to_plot = 2 * abs(fft)

# Plot the fft
plt.figure(2)
plt.loglog(freq, fft_to_plot)
plt.xlabel("frequency [Hz]", fontsize=20)
plt.ylabel("log spectrum magnitude", fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('FFT', fontsize=20)
define_plot_resolution()
plt.savefig('fft.png')
# plt.show()


""" Define the window used for the PSD estimation"""
# define the number of point for the window (choose arbitrary value this is just to plot the shape)
n_window = len(y_arr)

hann_win = hann(n_window)

# plot to check that the window looks good
plt.figure(3)
plt.plot(hann_win)
plt.title('Hann window', fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
define_plot_resolution()
plt.savefig('Hann_window.png')
# plt.show()

# plot signal with hann window
fft_hann = np.fft.fft(y_arr * hann_win) / fs

fft_to_plot_hann = 2 * abs(fft_hann)

plt.figure(4)
plt.loglog(freq, fft_to_plot_hann)
plt.xlabel("frequency [Hz]", fontsize=20)
plt.ylabel("log spectrum magnitude", fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('FFT with Hann window', fontsize=20)
define_plot_resolution()
plt.savefig('fft_hann.png')
# plt.show()

# plot spectrum with negative frequencies
freq_negf = np.linspace(0, fs, len(x_arr))

fft_to_plot_negf = abs(fft_hann)

plt.figure(0)
plt.yscale('log')
plt.plot(freq_negf, fft_to_plot_negf)
plt.xlabel("frequency [Hz]", fontsize=20)
plt.ylabel("log spectrum magnitude", fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('FFT with negative frequencies', fontsize=20)
define_plot_resolution()
plt.savefig('fft_negf.png')
# plt.show()

# zoom on frequencies above 20Hz to compare with the paper
cut20 = int(20 * (len(freq_negf) / fs))

plt.figure(5)
plt.xscale('log')
plt.yscale('log')
plt.plot(freq_negf[cut20:], fft_to_plot_negf[cut20:])
plt.xlabel("frequency [Hz]", fontsize=20)
plt.ylabel("log spectrum magnitude", fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('FFT with negative frequencies', fontsize=20)
define_plot_resolution()
plt.savefig('fft_negf_zoom.png')
# plt.show()

# inverse ft
inverse_fft = np.fft.ifft(fft_hann)

plt.figure(6)
plt.plot(x_arr, np.real(inverse_fft))
plt.title('Signal after Hann window', fontsize=20)
plt.xlabel('gps time [s]', fontsize=20)
plt.ylabel('amplitude', fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
define_plot_resolution()
plt.savefig('inverse_fft.png')
# plt.show()

"""Compute the PSD (median method)"""


def spectrumASD2s(signal, t0):
    """This function returns the ASD of a 2s interval"""
    y = signal[t0 * fs:(t0 + 2) * fs]

    n_window = len(y)
    hann_w = hann(n_window)

    fft = np.fft.rfft(y * hann_w)

    return abs(2 * fft / fs)


# storage of each 2s interval's ASD in matrices
fre = np.linspace(0, fs / 2, fs + 1)
plt.figure(7)
ASD1 = np.zeros((len(fre), int(((x_arr[-1] - x_arr[0]) / 2))))
ASD2 = np.zeros((len(fre), int(((x_arr[-1] - x_arr[0]) / 2))))
for i in range(int(((x_arr[-1] - x_arr[0]) / 2))):  # loop over the number of 2s interval
    A1 = spectrumASD2s(y_arr, 2 * i)
    A2 = spectrumASD2s(y_arr, 2 * i + 1)  # here the intervals are shifted of 1s
    plt.loglog(fre, A1, color='blue')
    plt.loglog(fre, A2, color='blue')
    for j in range(len(fre)):
        ASD1[j][i] = A1[j]
        ASD2[j][i] = A2[j]

# storage of the ASD values (mean of the medians) in a file to use them for the whitening
f = open("asdH.txt", "w+")
mean = []
for i in range(len(fre)):
    med_ASD1 = np.median(ASD1[i])
    med_ASD2 = np.median(ASD2[i])
    mean.append((med_ASD1 + med_ASD2) / 2)
    f.write("%s\n" % mean[i])
f.close()

plt.loglog(fre, moy, color='red')
plt.xlabel("frequency [Hz]", fontsize=20)
plt.ylabel("log spectrum magnitude", fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('ASD', fontsize=20)
define_plot_resolution()
plt.savefig('asdH.png')
# plt.show()
