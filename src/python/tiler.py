import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from math import *
import lib_white as white


def define_plot_resolution():
    """
    This function allow to define the resolution of a matplotlib plot on a way
    wich is device independent. Put this before saving any of your plot to get
    homogeneous resolution.
    """

    fig = plt.gcf()  # get current figure

    DPI = fig.get_dpi()
    fig.set_size_inches(1920.0 / float(DPI), 1080.0 / float(DPI))

    return


def bisquare(f, phi, Q):
    """This function returns the value of the bisquare window with its normalisation."""

    Wb = sqrt(315 / (128 * sqrt(11))) * sqrt(Q / phi)
    df = sqrt(11) * phi / Q
    w = 0

    if abs(f) < df:
        w = Wb * (1 - (f / df) ** 2) ** 2

    return w


def nextPowerOf2(n):
    """
    This function return the closest power of 2 bigger than n
    """

    power = ceil(log2(n))

    return power


class spectrogram:
    """
    Dedicated class to build a spectrogram with a Q plan methode
    """

    def __init__(self, Q, mu_max):

        self.Q = Q
        self.mu_max = mu_max
        self.fs = 1024

        self.freq_min = 20
        self.freq_max = self.fs / 2 / (1 + sqrt(11) / self.Q)
        self.freq_range = [self.freq_min, self.freq_max]

    def def_time_range(self, time_range):

        self.time_range = time_range

        return

    def Q_bin_numb(self):
        # define the Number of Q bins

        self.N_q = 1

        return

    def freq_bin_numb(self):
        # define the Number of frequency bins

        s_phi = (sqrt(2 + self.Q ** 2) / 2) * log(self.freq_max / self.freq_min)
        self.N_phi = ceil(s_phi / (2 * sqrt(self.mu_max / 3)))

        return

    def freqtiles_center_array(self):
        """
        This function define the center of each frequency bins and store it in a array
        
        """

        freq = []
        for i in range(self.N_phi):
            power = (0.5 + i) / self.N_phi
            freq.append(self.freq_min * pow((self.freq_max / self.freq_min), power))

        self.freq_array = freq

        return

    def time_bin_numb(self):
        """
        This function define the number of tiles needed for each frequency bins (stored in a list) 
        
        WARNING: self.N_t is a list

        """
        nb = []
        for i in range(self.N_phi):
            s_t = (2 * pi * self.freq_array[i] / self.Q) * (self.time_range[-1] - self.time_range[0])
            temp_Nt = s_t / (2 * sqrt(self.mu_max / 3))
            nb.append(2 ** nextPowerOf2(temp_Nt))

        self.N_t = nb

        return

    def build_Q_plan_row(self):
        """
        This function build a list of empty row, The size of each row 
        correspond to the size needed in the final Q plan 

        self.Q_plan_matrix_rows is a list of array
        """

        Q_plan_matrix_rows = []

        for i in range(len(self.N_t)):
            Q_plan_matrix_rows += [np.empty((self.N_t[i]))]

        self.Q_plan_matrix_rows = Q_plan_matrix_rows

        return

    def build_Q_plan_matrix(self):
        """
        Build a empty matrix with the highest resolution of the Q plan
        """

        self.Q_plan_matrix = np.empty((len(self.Q_plan_matrix_rows), len(self.Q_plan_matrix_rows[-1])))

        return

    def fill_row_for_demo(self):
        """
        This function fill the Q plan matrix with dummies values just to show its structure
        """

        # fill the rows with dummies values (0 or 1 alternatively)
        for i in range(int(np.shape(self.Q_plan_matrix)[0])):
            r = int(self.N_t[-1] / self.N_t[i])  # size of a tile with respect to time for a given phi
            for j in range(int(self.N_t[i] / 2)):
                self.Q_plan_matrix[i][2 * j * r:(2 * j + 1) * r] = np.zeros(r)
                self.Q_plan_matrix[i][(2 * j + 1) * r:(2 * j + 2) * r] = np.ones(r)

        return

    def fill_matrix(self, signal):
        """
        This function fill the Q plan matrix with the SNR values in each cell
        """
        tmin = self.time_range[0]
        tmax = self.time_range[-1]

        for i in range(len(signal)):

            interp = interp1d(np.linspace(-2, 2, len(signal[i])), signal[i])  # interpolation of the signal to fill the right tiles in our tiled matrix
            r = int(self.N_t[-1] / self.N_t[i])  # size of a tile with respect to time for a given phi

            for m in range(self.N_t[i]):
                t = tmin + (m + 0.5) * (tmax - tmin) / self.N_t[i]  #
                self.Q_plan_matrix[i][m * r:(m + 1) * r] = sqrt(abs(interp(t) ** 2 - 2))

        return

    def plot_matrix(self):
        """
        This function plot the Q plan matrix
        
        Need to be customized
        """

        plt.figure(1)
        extent = self.time_range[0], self.time_range[-1], self.freq_range[0], self.freq_range[-1]
        plt.imshow(self.Q_plan_matrix, origin='lower', aspect='auto', extent=extent, interpolation='none')
        plt.yscale('log')
        plt.xlabel("Time [s]", fontsize=20)
        plt.ylabel("Frequency [Hz]", fontsize=20)
        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
        plt.title("Spectrogram", fontsize=20)
        plt.colorbar()
        define_plot_resolution()
        plt.savefig('spectrogrammeH.png')
        plt.show()
        return


############ MAIN ################

# Define the fs
fs = 1024

# Define the Q
Q = 8.253

# Define the minimal energy loss at 20%
mu_max = 10 / 100

spectro = spectrogram(Q, mu_max)
spectro.def_time_range([-2, 2])
spectro.freq_bin_numb()
spectro.freqtiles_center_array()
spectro.time_bin_numb()
spectro.build_Q_plan_row()
spectro.build_Q_plan_matrix()
# spectro.fill_row_for_demo()

# we get back the whitened spectrum of the gravitational wave
w = white.whitening()
t0 = w[0]
print(t0)
xwh = w[1]

#the whitened spectrum gets shifted, multiplied by the bisquare window and then inverse Fourier transformed
Xwh = []
for i in range(spectro.N_phi):

    shift = int(spectro.freq_array[i] * (2 * len(xwh) / fs))  # position in the list of our first shifted frequency
    xwh_shift = xwh[shift:]  # shifting of our spectrum

    freq = np.linspace(0, fs / 2 - spectro.freq_array[i], len(xwh_shift))  # we don't have value above fs/2 so our window won't go above fs/2-phi
    window = []
    for j in range(len(freq)):
        window.append(bisquare(freq[j], spectro.freq_array[i], spectro.Q))

    Xwh.append(np.fft.irfft(xwh_shift * window))

# the tiled grid is filled with the SNR values
spectro.fill_matrix(Xwh)
spectro.plot_matrix()
