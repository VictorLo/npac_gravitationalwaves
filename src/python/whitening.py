import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import tukey
from scipy.interpolate import interp1d
from math import *

"""With this file we whiten our signal"""


def define_plot_resolution():
    """
    This function allow to define the resolution of a matplotlib plot on a way
    wich is device independent. Put this before saving any of your plot to get
    homogeneous resolution.
    """

    fig = plt.gcf()  # get current figure

    DPI = fig.get_dpi()
    fig.set_size_inches(1920.0 / float(DPI), 1080.0 / float(DPI))

    return


# This commande turn matplotlib interactive mode off.
# Plot will not be desplayed as long as it is activate
# Note that plot are still saved with it
plt.ioff()

# point out the root file
# root_file = ROOT.TFile("/home/ducoin/gravitational-waves.git/data/GW150914/l1.data.07.root","open")
file = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.00.npy")
file1 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.01.npy")
file2 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.02.npy")
file3 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.03.npy")
file4 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.04.npy")
file5 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.05.npy")
file6 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.06.npy")
file7 = np.load("/Users/Zaldric/Desktop/Cours/NPAC/Info/gravitational-waves/data/GW150914/h1.data.07.npy")

# get x and y axis
x_buff = np.concatenate((file[0], file1[0], file2[0], file3[0], file4[0], file5[0], file6[0], file7[0]), axis=None)
y_buff = np.concatenate((file[1], file1[1], file2[1], file3[1], file4[1], file5[1], file6[1], file7[1]), axis=None)

# convert to numpy array (starting from now you can forget that ROOT exist)
x_arr = np.array(x_buff)
y_arr = np.array(y_buff)

# define the sampling frequency [Hz]
fs = 1024

"""Whiten the spectrum"""


def spectrum4s(signal, t0):
    """This function returns the FT of a 4s interval"""
    y = signal[t0 * fs:(t0 + 4) * fs]

    n_window = len(y)
    tukey_w = tukey(n_window, 0.5)

    fft = np.fft.rfft(y * tukey_w)

    return fft


# get back the stored ASD values
f = open("asdH.txt", 'r')
N = f.readlines()
f.close()

n = []
for i in range(len(N)):
    n.append(float(N[i]))

# creation of an interpolated function to get the ASD value for any frequency
fre_asd = np.linspace(0, fs / 2, len(n))
interp = interp1d(fre_asd, n)

fre = np.linspace(0, fs / 2, 2 * fs + 1)
cut20 = 20 * int(len(fre) / fre[-1])  # position of the 20Hz frequency in the list
spectra = np.zeros((int(((x_arr[-1] - x_arr[0]) / 2)),
                    len(fre)),
                   dtype=np.dtype(complex))  # matrice used to store a whitened spectrum for each 4s interval
for i in range(int(((x_arr[-1] - x_arr[0]) / 2))):  # loop to move 2s by 2s
    A1 = spectrum4s(y_arr, 2 * i)
    new_A1 = np.zeros(len(fre), dtype=np.dtype(complex))
    new_A1[cut20:] = A1[cut20:]  # all frequencies below 20Hz go to zero
    A1_white = sqrt(2) * new_A1 / interp(fre)  # whitening of the signal
    for j in range(len(fre)):
        spectra[i][j] = A1_white[j]

plt.figure(1)
plt.plot(fre, 2 * abs(spectra[0]) / fs, color='blue')
plt.xlabel("frequency [Hz]", fontsize=20)
plt.ylabel("spectrum magnitude", fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('Whitened spectrum', fontsize=20)
define_plot_resolution()
plt.savefig('whitened_spectrum.png')
# plt.show()

"""Inv fourier transf of the whitened spectrum"""
x_t = np.linspace(0, 4, 4 * fs)
y_t = np.real(np.fft.irfft(spectra[0]))

plt.figure(2)
plt.plot(x_t, y_t, color='blue')
plt.xlabel("time [s]", fontsize=20)
plt.ylabel("amplitude", fontsize=20)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title('Whitened signal', fontsize=20)
define_plot_resolution()
plt.savefig('whitened_amplitude.png')
plt.show()
